# ClickMouse

An automatic mouse clicker. 

You can enable and disable the mouse clicking action using the -fixed-- shortcut key listed in the window.

There is currently a one second delay after clicking the button or hitting the command key, and start of the click events. This is to prevent disabling the button immediately. 
Perhaps I will make this a setting as well if I need it.

The number of clicks per second can be set between 0 and 20 cps with the slider at the bottom of the window.


## Permissions
### Accessibility
To enable the injection of mouse clicks in the system event queue, you need to drag the app into the accessibility pane of the Privacy settings of the System Preferences:
![Accessibility screenshot](images/accessibility.png)

### Input Monitoring
To activate and de-activate clicking using the keyboard shortcuts, you need to give the app access to monitor the keyboard input. It will do this only as long as the app is active.
![Accessibility screenshot](images/inputmonitoring.png)


## To Do
- [] Not all mouse events are gobbled up
	- 	add logging to all received events on gobbling
- [] nicer UI for requesting accessibility permissions
- [x] add override to max clicks (currently 20/s)


