//
//  ContentView.swift
//  ClickMouse
//
//  Created by Axel Roest on 07-08-2020.
//  Copyright © 2020 Phluxus. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
    static let defaultClicks = 20.0
    @ObservedObject var clicking: Clicking
    @Binding var clicksPerSecond: Double
    @State var maxClicks: Double = defaultClicks
    @State var maxClicksString = String(format: "%.0f", defaultClicks) {
        didSet {
            self.maxClicks = Double(maxClicksString) ?? ContentView.defaultClicks
        }
    }
    
    var body: some View {
        ZStack {
            if clicking.isClicking {
                Color(.systemGreen)
            } else {
                Color(.systemTeal)
            }
            VStack(alignment: .center, spacing: 30) {
                Spacer()
                Button(action: {
                    print("BUTTON ACTION : TOGGLE")
                    self.clicking.toggle()
                }, label: {
                    if clicking.isClicking {
                        Text("Clicking")
                    } else {
                        Text("Stopped")
                    }
                }).padding(.bottom, 40)
                infoView
                inputView
                Slider(value: $clicksPerSecond, in: 0.2...maxClicks) {
                    return Text("CPS: \(Int(clicksPerSecond))")
                }.padding(32)
            }
        }
    }
}

private extension ContentView {
    var infoView: some View {
        VStack {
            Text("location: \(clicking.currentPosition)")
                .multilineTextAlignment(.leading)
                .padding()
            Spacer()//.frame(maxWidth: .infinity, maxHeight: .infinity)
            Text("To start: ctrl-alt-§")
            Text("to stop: ctrl-§")
                .padding()
        }
    }
    
    var inputView: some View {
        HStack {
            Spacer()
            TextField("Max clicks value",
                      value: $maxClicks,
                      format: .number)
            .frame(width: 50)
            Spacer()
        }
    }
}
//
//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView(clicking: Clicking(soundPlayer: SoundPlayer()), clicksPerSecond: .constant(10))
//    }
//}

class SoundPlayerMock: SoundPlayerProtocol {
    func playStopSound() {}
    func playStartSound() {}
}

#Preview("Main") {
    ContentView(clicking: Clicking(soundPlayer: SoundPlayerMock()), clicksPerSecond: .constant(10))
}
