//
//  Clicking.swift
//  ClickMouse
//
//  Created by Axel Roest on 07-08-2020.
//  Copyright © 2020 Phluxus. All rights reserved.
//

import Foundation
import AppKit
import Combine
import CoreGraphics
import AudioToolbox
import SwiftUI

// I noticed there are TWO timer objects. This is weird, so perhaps Clicking is instantiated twice? Or we should not store timer in a property?

class Clicking: ObservableObject {
    let DEBUG = false
    let soundPlayer: SoundPlayerProtocol
    let clickPulseTime = 5
    var lastLocation: CGPoint = CGPoint(x: 0, y: 0) {
        didSet {
            let x = Int(lastLocation.x)
            let y = Int(lastLocation.y)
            self.currentPosition = "[ \(x) , \(y) ]"
        }
    }
    let startDelay = 2  //  seconds, stop is immediate
    @Published var clicksPerSecond: Double = 20
    
    @Published var currentPosition: String = "[ , ]"
    @Published var isClicking = false

    var cancellables = Set<AnyCancellable>()
    var clickTimer: Timer?
    
    init(soundPlayer: SoundPlayerProtocol) {
        self.soundPlayer = soundPlayer
        setupBindings()
        setupNotifications()
    }
    
    func toggle() {
        isClicking.toggle()
        
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(enableClicking), name: NSNotification.Name("clickerOn"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disableClicking), name: NSNotification.Name("clickerOff"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(mouseMoved), name: NSNotification.Name("mousemoved"), object: nil)
    }

    @objc func enableClicking() {
//        guard self.lastLocation.x != 0 && self.lastLocation.y != 0 else {
//            return
//        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(startDelay)) {
            self.isClicking = true
            self.soundPlayer.playStartSound()
        }
    }

    @objc func disableClicking() {
        self.isClicking = false
        self.soundPlayer.playStopSound()
    }
    
    @objc func mouseMoved(notification: NSNotification) {
        let dict: CFDictionary = notification.object as! CFDictionary
        guard let location = CGPoint(dictionaryRepresentation: dict) else {
            return
        }
        self.lastLocation = location
//        print("mouse moved to: \(location)")
    }
    
    func setupBindings() {
        $isClicking
            .receive(on: DispatchQueue.main)
            .sink { (value) in
                print("isClicking: \(value)")
                if value {
                    if self.clickTimer == nil {
                        self.startClicking()
                    }
                } else {
                    self.stopClicking()
                }

        }.store(in: &cancellables)
    }
    
    func startClicking() {
        let timeInterval: TimeInterval = 1.0 / clicksPerSecond
        self.clickTimer = Timer.init(timeInterval: timeInterval, repeats: true, block: { aTimer in
//            print("Fire timer: \(aTimer)")
            self.clickDownMouse()
        })
        print("startClicking timer: \(String(describing: self.clickTimer))")
        RunLoop.current.add(clickTimer!, forMode: RunLoop.Mode.default)
        print("StartClicking")
    }

    func stopClicking() {
        if let clicker = self.clickTimer {
            clicker.invalidate()
            print("Stopping \(clicker) … ", terminator: "")

        }
        self.clickTimer = nil
        print("Stopped")
    }
    
    func clickDownMouse() {
//        print("click DOWN: \(Date()), timer: \(clickTimer.debugDescription)")
        let leftMouseDownEvent = CGEvent(mouseEventSource: nil, mouseType: .leftMouseDown, mouseCursorPosition: lastLocation, mouseButton: .left)
        leftMouseDownEvent?.post(tap: .cghidEventTap)          //   cgAnnotatedSessionEventTap
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(clickPulseTime)) {
            self.clickUpMouse()
        }
    }

    func clickUpMouse() {
//        print("click UP: \(Date())")
        let leftMouseUpEvent = CGEvent(mouseEventSource: nil, mouseType: .leftMouseUp, mouseCursorPosition: lastLocation, mouseButton: .left)
        leftMouseUpEvent?.post(tap: .cghidEventTap)            //
    }
}

// to flush system buffer:
// CGEvent, listen to mousedown,
// filter in eventtap, then don't forward the event, so to 'absorb' it.
// I do the same with the keyboard read event.
