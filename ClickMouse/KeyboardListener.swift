//
//  KeyboardListener.swift
//  ClickMouse
//
//  Created by Axel Roest on 07-08-2020.
//  Copyright © 2020 Phluxus. All rights reserved.
//

import ApplicationServices
import Foundation
import CoreGraphics
import CoreFoundation
import Combine

/* examples
 https://github.com/neilco
 https://github.com/SkrewEverything/Swift-Keylogger
 https://stackoverflow.com/questions/31891002/how-do-you-use-cgeventtapcreate-in-swift
 https://stackoverflow.com/questions/709526/difference-between-cfrunloopremovesource-and-cfrunloopsourceinvalidate
 https://developer.apple.com/documentation/corefoundation/1543356-cfrunloopaddsource
 */


/// This callback function receives events from the main event queue and fires of notifications for
/// either keyboard or mousemoved events
func myCGEventCallback(proxy: CGEventTapProxy, type: CGEventType, event: CGEvent, refcon: UnsafeMutableRawPointer?) -> Unmanaged<CGEvent>? {
    let keyCode = 10   // paragraph sign

    if [.mouseMoved].contains(type) {
        let location = event.location
        let locRef = location.dictionaryRepresentation
        NotificationCenter.default.post(name: NSNotification.Name("mousemoved"), object: locRef)
    }
    
    if [.keyDown , .keyUp].contains(type) {
        let keyCode = event.getIntegerValueField(.keyboardEventKeycode)
//        NSLog("key: \(keyCode)")
        let modifiersOff = event.flags.contains(CGEventFlags.maskControl)
        let modifiersOn = event.flags.contains([CGEventFlags.maskControl, CGEventFlags.maskAlternate])
        if keyCode == keyCode && modifiersOn {
            NotificationCenter.default.post(name: NSNotification.Name("clickerOn"), object: nil)
            return nil
        }
        else if keyCode == keyCode && modifiersOff {
            NotificationCenter.default.post(name: NSNotification.Name("clickerOff"), object: nil)
            return nil
        }
    }
    return Unmanaged.passRetained(event)
}


// ToDo, if the first one is a mouseDown, go right ahead and gobble.
// However, if the first one is a mouseUp, let it pass through, so the mouse behaves normally!
var receivedUps = 0
var receivedDowns = 0

func gobbleUpMouseclicks(proxy: CGEventTapProxy, type: CGEventType, event: CGEvent, refcon: UnsafeMutableRawPointer?) -> Unmanaged<CGEvent>? {
    
    if [.leftMouseDown, .leftMouseUp].contains(type) {
        if type == .leftMouseUp {
            receivedUps += 1
        }
        if type == .leftMouseDown {
            receivedDowns += 1
        }
        guard !gobblingJustStarted else {
            gobblingJustStarted = false
            if type == .leftMouseUp {
                print("pass UP mouse click")
                return Unmanaged.passRetained(event)
            }
            print("purge mouse click gobblingJustStarted")
            return nil
        }
        print("purge mouse click")
        KeyboardListener.resetTimer()
        return nil
    } else {
        print("not gobble up something else")
    }

    return Unmanaged.passRetained(event)
}

var mouseClickTimer: Timer?
var gobblingMouseClicks = false
var gobblingJustStarted = false

class KeyboardListener {
    var keyboardListener: CFRunLoopSource?
    static var mouseClickListener: CFRunLoopSource?
    var tappingEnabled = false

    func setup() {

        tappingEnabled = acquirePrivileges()
        guard tappingEnabled else {
            return
        }
        NotificationCenter.default.addObserver(self, selector: #selector(disableClicking), name: NSNotification.Name("clickerOff"), object: nil)
        
        guard let eventTap = createKeyboardMouseMovedTap() else {
                                                        print("failed to create event tap")
                                                        return
                                                    }
        
        let runLoopSource = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, eventTap, 0)
        CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, .commonModes)
        CGEvent.tapEnable(tap: eventTap, enable: true)
        keyboardListener = runLoopSource
        CFRunLoopRun()
    }
    
    public func removeListener() {
        guard let keyboardListener = keyboardListener else {
            return
        }
        if CFRunLoopContainsSource(CFRunLoopGetCurrent(), keyboardListener, .commonModes) {
            // Todo: use CFRunLoopSourceInvalidate instead of Remove,
            // see  https://stackoverflow.com/questions/709526/difference-between-cfrunloopremovesource-and-cfrunloopsourceinvalidate
            CFRunLoopRemoveSource(CFRunLoopGetCurrent(), keyboardListener, .commonModes)
            CFRunLoopSourceInvalidate(keyboardListener)
        }
    }
    
    @objc func disableClicking() {
        if !gobblingMouseClicks {
            gobblingMouseClicks = true
            startGobblingUpMouseclicks()
        }
    }
}

private extension KeyboardListener {
    
    /// acquirePrivileges could be made prettier with better GUI
    /// - Returns: true if we can access the even queue
    func acquirePrivileges() -> Bool {
        let trusted = kAXTrustedCheckOptionPrompt.takeUnretainedValue()
        let privOptions = [trusted: true] as CFDictionary
        let accessEnabled = AXIsProcessTrustedWithOptions(privOptions)

        if accessEnabled == true {
            print("we can create a tap")
        } else {
            print("tapping is disabled")
            print("Application is not a trusted Accessibility Client – crashing now^^")
            print("    (For ClickMouse to work you need to add/enable it in \"System Preferences -> Security & Privacy -> Accessibility\")")
        }
        return accessEnabled
    }
}

private extension KeyboardListener {
    static func resetTimer() {
        mouseClickTimer?.invalidate()
        mouseClickTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { aTimer in
            stopGobblingUp()
        })
    }

    static func stopGobblingUp() {
        print("STOP gobbling up mouse clicks")
        guard let mouseClickListener = KeyboardListener.mouseClickListener else {
            return
        }
        if CFRunLoopContainsSource(CFRunLoopGetCurrent(), mouseClickListener, .commonModes) {
            CFRunLoopRemoveSource(CFRunLoopGetCurrent(), mouseClickListener, .commonModes)
        }
        print("Received mouseDowns: \(receivedDowns)")
        print("Received mouseUps: \(receivedUps)")
        gobblingMouseClicks = false
    }
    
    func startGobblingUpMouseclicks() {

        print("START gobbling up mouse clicks")
        receivedUps = 0
        receivedDowns = 0
        // add the extra eventtap
        guard let eventTap = createMouseClickGobblerTap() else {
            print("failed to create mouseclick event tap")
            return
        }
        let runLoopSource = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, eventTap, 0)
        CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, .commonModes)
        CGEvent.tapEnable(tap: eventTap, enable: true)
        KeyboardListener.mouseClickListener = runLoopSource
        print("added mouseclick gobbler event tap")

        // wait until no more clicks appear within a second (less?) -> then remove the tap
//        CGEvent.tapEnable(tap: eventTap, enable: false)
    }
    
    private func createMouseClickGobblerTap() -> CFMachPort? {
        let eventMask = (1 << CGEventType.leftMouseUp.rawValue) | (1 << CGEventType.leftMouseDown.rawValue)
        return CGEvent.tapCreate(tap: .cgSessionEventTap,
                                               place: .headInsertEventTap,
                                               options: .defaultTap,
                                               eventsOfInterest: CGEventMask(eventMask),
                                               callback: gobbleUpMouseclicks,
                                               userInfo: nil)
    }
    
    private func createKeyboardMouseMovedTap() -> CFMachPort? {
        let eventMask = (1 << CGEventType.keyDown.rawValue) | (1 << CGEventType.keyUp.rawValue) | (1 << CGEventType.mouseMoved.rawValue)
        return CGEvent.tapCreate(tap: .cgSessionEventTap,
                                               place: .headInsertEventTap,
                                               options: .defaultTap,
                                               eventsOfInterest: CGEventMask(eventMask),
                                               callback: myCGEventCallback,
                                               userInfo: nil)
    }
}
