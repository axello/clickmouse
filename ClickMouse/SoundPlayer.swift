//
//  SoundPlayer.swift
//  ClickMouse
//
//  Created by Axel Roest on 10-08-2020.
//  Copyright © 2020 Phluxus. All rights reserved.
//

import Foundation
import AVKit

protocol SoundPlayerProtocol {
    func playStartSound()
    func playStopSound()
}

class SoundPlayer: SoundPlayerProtocol {
    ///The directories where sound files are located.
    let rootSoundDirectories: [String] = ["/Users/axel/Library/Sounds", "/System/Library/Sounds"]
    var avPlayer: AVAudioPlayer?
    
    ///Array to hold directories when we find them.
    var directories: [String] = []
    
    ///Tuple to hold directories and an array of file names within.
    var soundFiles: [(directory: String, files: [String])] = []
    

    private var startSound: String = "/System/Library/Sounds/Tink.aiff"
    private var stopSound: String = "/System/Library/Sounds/Basso.aiff"
    
    init() {
        getDirectories()
        loadSoundFiles()
//        print("sounds: \(soundFiles)")
        playSound(soundPath: startSound)
    }
    
    public func playStartSound() {
        playSound(soundPath: startSound)
    }

    public func playStopSound() {
        playSound(soundPath: stopSound)
    }

    //Starting with the "/Library/Ringtones" & "/System/Library/Audio/UISounds" directories, it looks for other sub-directories just one level lower and saves their relative path in directories array.
    //- URLs: All of the contents of the directory (files and sub-directories).
    func getDirectories() {
        let fileManager: FileManager = FileManager()
        for directory in rootSoundDirectories {
            let directoryURL: URL = URL(fileURLWithPath: "\(directory)", isDirectory: true)
            directories.append(directory)
            let newSoundFile: (directory: String, files: [String]) = (directory, [])
            soundFiles.append(newSoundFile)
            do {
                let URLs: [URL] = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: FileManager.DirectoryEnumerationOptions())
                var urlIsaDirectory: ObjCBool = ObjCBool(false)
                for url in URLs {
                    if fileManager.fileExists(atPath: url.path, isDirectory: &urlIsaDirectory) {
                        if urlIsaDirectory.boolValue {
                            let directory: String = "\(url.relativePath)"
                            let files: [String] = []
                            let newSoundFile: (directory: String, files: [String]) = (directory, files)
                            directories.append("\(directory)")
                            soundFiles.append(newSoundFile)
                        }
                    }
                }

            } catch {
                debugPrint("\(error)")
            }
        }
    }
    
    //For each directory, it looks at each item (file or directory) and only appends the sound files to the soundfiles[i]files array.
    //- URLs: All of the contents of the directory (files and sub-directories).
    func loadSoundFiles() {
        for i in 0...directories.count-1 {
            let fileManager: FileManager = FileManager()
            let directoryURL: URL = URL(fileURLWithPath: directories[i], isDirectory: true)
    
            do {
                let URLs: [URL] = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: [URLResourceKey.isDirectoryKey], options: FileManager.DirectoryEnumerationOptions())
                var urlIsaDirectory: ObjCBool = ObjCBool(false)
                for url in URLs {
                    if fileManager.fileExists(atPath: url.path, isDirectory: &urlIsaDirectory) {
                        if !urlIsaDirectory.boolValue {
                            soundFiles[i].files.append("\(url.lastPathComponent)")
                        }
                    }
                }
            } catch {
                debugPrint("\(error)")
            }
        }
    }

    //Play the sound
    func playSound(soundPath: String) {
        let fileURL: URL = URL(fileURLWithPath: soundPath)
        do {
            let player = try AVAudioPlayer(contentsOf: fileURL)
            player.play()
            avPlayer = player
        } catch {
            debugPrint("\(error)")
        }
    }

    func playSound(directory: String, sound: String) {
    //Play the sound
//        let directory: String = soundFiles[indexPath.section].directory
//        let fileName: String = soundFiles[indexPath.section].files[indexPath.row]
        let fileURL: URL = URL(fileURLWithPath: directory).appendingPathComponent(sound)
        do {
            let player = try AVAudioPlayer(contentsOf: fileURL)
            player.play()
            avPlayer = player
        } catch {
            debugPrint("\(error)")
        }
    }
//NSString *path = [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"] pathForResource:@"Tock" ofType:@"aiff"];
//SystemSoundID soundID;
//AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath:path], &soundID);
//AudioServicesPlaySystemSound(soundID);
//AudioServicesDisposeSystemSoundID(soundID);
//
}
//
//sounds: [(directory: "/Users/axel/Library/Sounds", files: ["Drill", "Yeehaw", "Voltage", "Roll and Crash", "Crowd Laugh", "Warp Down", "Man Laugh", "Sosumi", "Pong2003", "Bongo", "Warp Up", ".DS_Store", "Vibro Up", "Karate", "Fly Out", "Arrow.wav", "Doorbell", "MailHubAlarmSound.aif", "Magic Bell.aiff", "Opening", "Drum", "Purr", "Stapler", "chime.aiff", "Take Off", "SimpleBeep16", "Click", "Here It Is", "Submarine", "Surprise", "Indigo", "Pot Drop", "Glass", "Logjam", "Pong", "Click.aiff", "Wood Bonk", "Click1.mp3", "Bongo.aiff", "Bongo1.mp3", "Gun", "Cymbal", "Uh oh", "Plane", "Tick Tock", "Phaser", "String", "Horn", "Bubbles", "Ring", "ChuToy", "Shave and a Haircut", "Quack", "Temple", "Pencil Check", "Fly In", "Ray", "Doorbell1.mp3", "Ooooh", "Yahoo", "Doorbell.aiff", "Laugh"]), (directory: "/Users/axel/Library/Sounds/AIM®", files: []), (directory: "/Users/axel/Library/Sounds/MS Sounds", files: [".DS_Store"]), (directory: "/System/Library/Sounds", files: ["Submarine.aiff", "Ping.aiff", "Purr.aiff", "Hero.aiff", "Funk.aiff", "Pop.aiff", "Basso.aiff", "Sosumi.aiff", "Glass.aiff", "Blow.aiff", "Bottle.aiff", "Frog.aiff", "Tink.aiff", "Morse.aiff"])]

