//
//  AppDelegate.swift
//  ClickMouse
//
//  Created by Axel Roest on 07-08-2020.
//  Copyright © 2020 Phluxus. All rights reserved.
//

import Cocoa
import SwiftUI

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var window: NSWindow!
    let keyListener = KeyboardListener()
    @ObservedObject var clicker = Clicking(soundPlayer: SoundPlayer())
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Create the SwiftUI view that provides the window contents.
        let contentView = ContentView(clicking: clicker, clicksPerSecond: $clicker.clicksPerSecond )
        keyListener.setup()
        
        // Create the window and set the content view. 
        window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 280, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.center()
        window.setFrameAutosaveName("Auto Clicker")
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        keyListener.removeListener()
    }
}
